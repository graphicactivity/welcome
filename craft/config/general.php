<?php

return array(
    '*' => array(
		'siteUrl' => null,
		'defaultWeekStartDay' => 0,
        'omitScriptNameInUrls' => true,
		'cpTrigger' => 'admin',
    ),

    'welcomeeatery.dev' => array(
        'devMode' => true,
        'environmentVariables' => array(
        	'basePath' => '/Users/GraphicActivity/Sites/welcomeeatery.dev/public/',
            'baseUrl'  => 'http://welcomeeatery.dev/',
        )
    ),

    'welcomeeatery.co.nz' => array(
        'devMode' => false,
        'environmentVariables' => array(
        	'basePath' => '/var/www/html/',
            'baseUrl'  => 'https://welcomeeatery.co.nz',
        )
    )
);

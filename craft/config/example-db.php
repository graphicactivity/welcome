<?php

return array(
    '*' => array(
        'tablePrefix' => 'craft',
        'server' => 'localhost',
    ),
    'welcomeeatery.dev' => array(
    	'database' => 'eatery',
        'user' => 'root',
        'password' => 'xxx',
    ),
    'welcomeeatery.co.nz' => array(
    	'database' => 'eatery',
        'user' => 'root',
        'password' => 'xxx',
    ),
);
